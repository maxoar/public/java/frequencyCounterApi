# Frequency Counter API


## Compile
> mvn clean install -e

## Run
> java -jar target\frequencyCounter.jar


<br/>

## Objetivo
Contar la frequency de aparición del tópico consultado (palabra/word), dentro del html que devuelve como respuesta la api de Wikipedia.
Wikipedia API: https://en.wikipedia.org/w/api.php


## Frequency Counter API
http://localhost:8082/api/wikipedia/freq/${topic}

 