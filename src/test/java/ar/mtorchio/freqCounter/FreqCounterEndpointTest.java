package ar.mtorchio.freqCounter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.Matchers.is;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class FreqCounterEndpointTest {

    private static final String HOST = "http://localhost:8280";
    private static final String FREQUENCY_API = "/api/wikipedia/freq/";

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testApi_Ok() throws Exception {
        //Al momento de la realización de la API la frequencia es 173 (https://regex101.com/r/BQYXG5/2).
        mockMvc.perform(get(HOST+FREQUENCY_API + "/google"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("google")))
                .andExpect(jsonPath("$.frequency", is(173)));
    }

    @Test
    public void testApi_NotFound() throws Exception {
        //Al momento de la realización de la API el tópico a buscar no existe.
        mockMvc.perform(get(HOST+FREQUENCY_API + "/sarasaaasas"))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.httpStatus", is("NOT_FOUND")))
                .andExpect(jsonPath("$.message", is("Topic not found")))
                .andExpect(jsonPath("$.debugMessage", is("TopicNotFoundException [topic=sarasaaasas]")));
    }

}
