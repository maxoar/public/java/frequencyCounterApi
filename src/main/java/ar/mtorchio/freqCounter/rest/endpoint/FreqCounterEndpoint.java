package ar.mtorchio.freqCounter.rest.endpoint;

import ar.mtorchio.freqCounter.bo.FreqCounterBo;
import ar.mtorchio.freqCounter.entity.Word;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@RestController
@RequestMapping("/api")
public class FreqCounterEndpoint {

    @Autowired
    FreqCounterBo freqCounterBo;

    @GetMapping("/wikipedia/freq/{topic}")
    public ResponseEntity<Word> countFrequencyOcurrenceOfTopic(@PathVariable("topic") String topic) {
        if( Objects.isNull(topic) ) throw new IllegalArgumentException("The received parameter is invalid or not supplied");
        Word word = freqCounterBo.countFrequencyOfTopicWikipedia(topic);
        return ResponseEntity.ok().body( word );
    }

}
