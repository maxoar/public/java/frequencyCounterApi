package ar.mtorchio.freqCounter.rest.exceptions;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@NoArgsConstructor
@AllArgsConstructor
public class TopicNotFoundException extends RuntimeException {

    @NonNull
    private String topic;

    @Override
    public String toString(){
        return "TopicNotFoundException [topic="+topic+"]";
    }

}

