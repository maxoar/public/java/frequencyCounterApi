package ar.mtorchio.freqCounter.rest.handler;

import ar.mtorchio.freqCounter.entity.RestException;
import ar.mtorchio.freqCounter.rest.exceptions.TopicNotFoundException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler{// extends ResponseEntityExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<RestException> handleException(Throwable ex) {
        String error = "Internal Server Error";
        return buildResponseEntity( new RestException( HttpStatus.INTERNAL_SERVER_ERROR, error, ex.getLocalizedMessage() ) );
    }

    @ExceptionHandler(TopicNotFoundException.class)
    public ResponseEntity<RestException> exceptionTopicNotFoundException(TopicNotFoundException ex) {
        String error = "Topic not found";
        return buildResponseEntity(new RestException(HttpStatus.NOT_FOUND, error, ex.toString()));
    }

    private ResponseEntity<RestException> buildResponseEntity(RestException apiError) {
        return new ResponseEntity<>(apiError, apiError.getHttpStatus());
    }
}
