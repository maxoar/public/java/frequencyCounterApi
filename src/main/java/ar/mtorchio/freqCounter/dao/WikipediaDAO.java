package ar.mtorchio.freqCounter.dao;

import ar.mtorchio.freqCounter.dto.WikipediaTopicResponseDTO;
import ar.mtorchio.freqCounter.mapper.WikipediaResponseMapper;
import ar.mtorchio.freqCounter.rest.exceptions.TopicNotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;

@Repository
public class WikipediaDAO {

    private static final String WIKI_TOPIC_URL = "https://en.wikipedia.org/w/api.php?action=parse&section=0&prop=text&format=json&page=";
    private static final String WIKI_ERROR_HEADER = "MediaWiki-API-Error";
    private static final Logger logger = LogManager.getLogger(WikipediaDAO.class);

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private WikipediaResponseMapper wikipediaResponseMapper;

    public WikipediaTopicResponseDTO getTopicInfo(String topic) {
        String url = new StringBuilder(WIKI_TOPIC_URL).append(topic).toString();
        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
        if( ! apiError(response) ) throw new TopicNotFoundException(topic);
        return wikipediaResponseMapper.mapResponse(response.getBody());
    }

    private boolean apiError( ResponseEntity response ){
        return Objects.isNull( response.getHeaders().get(WIKI_ERROR_HEADER) );
    }

}
