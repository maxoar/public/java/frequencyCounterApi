package ar.mtorchio.freqCounter.bo;

import ar.mtorchio.freqCounter.dao.WikipediaDAO;
import ar.mtorchio.freqCounter.dto.WikipediaTopicResponseDTO;
import ar.mtorchio.freqCounter.entity.Word;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Component
public class FreqCounterBo {

    @Autowired
    WikipediaDAO wikipediaDAO;

    public Word countFrequencyOfTopicWikipedia(String topic) {
        WikipediaTopicResponseDTO wikiResponse = wikipediaDAO.getTopicInfo(topic);
        String text = wikiResponse.getText();
        Pattern regex = ignoreCaseRegex(topic);
        Long frequency = countMatches(regex, text);
        return new Word(topic, frequency);
    }

    private Pattern ignoreCaseRegex(String str){
        StringBuilder sb = new StringBuilder("(?i)").append(str);  //Regex (?i)${str} - x ej. (?i)pizza
        return Pattern.compile(sb.toString());
    }

    private Long countMatches(Pattern regex, String text){
        AtomicLong matches = new AtomicLong(0);
        Matcher matcher = regex.matcher(text);
        while (matcher.find())
            matches.incrementAndGet();
        return matches.longValue();
    }

}
