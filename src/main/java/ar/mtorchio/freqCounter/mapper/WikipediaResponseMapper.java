package ar.mtorchio.freqCounter.mapper;

import ar.mtorchio.freqCounter.dto.WikipediaTopicResponseDTO;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Objects;

@Component
public class WikipediaResponseMapper {

    private static final String TITLE = "title";
    private static final String PAGE_ID = "pageid";
    private static final String TEXT = "text";
    private static final String HTML_TEXT = "*";

    public WikipediaTopicResponseDTO mapResponse(String response) {
        JsonParser springParser = JsonParserFactory.getJsonParser();
        Map<String, Object> resultMap = springParser.parseMap(response);
        Map<String, Object> info =
                (Map<String, Object>)  resultMap.get("parse");

        return convertToWikiResponse(info);
    }

    private WikipediaTopicResponseDTO convertToWikiResponse(Map<String, Object> wikiMap){
        String title = (String) wikiMap.get(TITLE);
        Integer pageid = (Integer) wikiMap.get(PAGE_ID);
        String textContent = (String) ( (Map<String, Object>) wikiMap.get(TEXT) ).get(HTML_TEXT);
        if( Objects.isNull(textContent) )
            throw new IllegalArgumentException("text field at Wikipedia API response must not be null.");
        return new WikipediaTopicResponseDTO(title, pageid, textContent);
    }
}
