package ar.mtorchio.freqCounter.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class Word implements Serializable {

    private String name;

    private Long frequency;


}
