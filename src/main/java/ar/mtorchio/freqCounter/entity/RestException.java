package ar.mtorchio.freqCounter.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class RestException implements Serializable {

    private HttpStatus httpStatus;
    private String message;
    private String debugMessage;

}
