package ar.mtorchio.freqCounter.dto;

import lombok.*;

//@Builder

@Data
@AllArgsConstructor
public class WikipediaTopicResponseDTO {

    private String title;

    private Integer pageid;

    @NonNull
    private String text;

}